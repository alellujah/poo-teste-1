# 1º - Teste modelo POO (12/11/2018)

**Contexto:**
A empresa PortNow gere um importante porto europeu. A empresa pretende criar um programa para gerir a sua operação corrente que envolve diferentes aspectos da actividade.

1. Crie uma classe que represente a entidade Embarcação e que contenha o porto de registo, a designação, a capacidade de carga (em toneladas) e o comprimento (em metros). A classe tem de incluir os métodos set e get respectivos.

2. Na classe anterior crie um construtor que receba os atributos todos como argumentos.

3. Na mesma classe crie um método toString() com a funcionalidade usual.

4. Na aplicação crie um array de embarcações com três elementos e preencha-o com objectos de exemplo.

5. Crie um método que imprima a lista das embarcações actuais.

6. Crie um método que retorne a embarcação com maior capacidade de carga presente no array.

7. Crie um método que retorne a média das capacidades de carga das embarcações presentes no array.

8. Pretende distinguir-se os navios de cruzeiro dos restantes visto ser necessário conhecer-se a lotação (em número de passageiros) destes navios. Programe o código necessário para tal.

## Extra

9. Obter a lista de barcos ordenada alfabeticamente. 
10. Criar um método para adicionar passageiros aos cruzeiros.