package portNow_revisoes;

public class Main {
	//5. Crie um método que imprima a lista das embarcações actuais.
	private static void imprimir(Embarcacao[] lista) {
		if (lista.length == 0) {
			System.out.println("Lista vazia");
		}
		else {
			for (int i = 0; i < lista.length; i++) {
				System.out.println(lista[i].toString());
			}	
		}		
	}
	
	//6. Crie um método que retorne a embarcação com maior capacidade de carga presente no array.	
	private static String maiorEmbarcacao(Embarcacao[] lista) {			
		if (lista.length == 0) {
			return "Lista vazia";
		}
		else {
			Embarcacao maior = lista[0];
			for (int i = 0; i < lista.length; i++) {
				if (maior.getComprimento() < lista[i].getComprimento()) {
					maior = lista[i];
				}
			}
			return "A maior embarcacao é: " + System.lineSeparator() + maior.toString();
		}		 
	}
	
	//7. Crie um método que retorne a média das capacidades de carga das embarcações presentes no array.
	private static String mediaCarga(Embarcacao[] lista) {		
		if (lista.length == 0) {
			return "Lista vazia";
		}
		else {
			double total = 0;
			for (int i = 0; i < lista.length; i++) {
				total += lista[i].getCarga();
			}
			return "A media das cargas é: " + (total / lista.length);	
		}		
	}
	
	//9. Obter a lista de barcos ordenada alfabeticamente.
	private static Embarcacao[] ordenarLista(Embarcacao[] lista) {
		int n = lista.length;
		for (int i = 0; i < n-1; i++) {
			for (int j = 0; j < n-i-1; j++) {
				if (lista[j].getPortoRegisto().compareTo(lista[j+1].getPortoRegisto()) > 0) { //compareTo - if the string is > 0 then is ahead of the original string that we compared 
					Embarcacao obtemp = lista[j];					
					lista[j] = lista[j+1];
					lista[j+1] = obtemp;														
				}
			}
		}
		return lista;
	}
		
	public static void main(String[] args) {
		//4. Na aplicação crie um array de embarcações com três elementos e preencha-o com objectos de exemplo.
		Embarcacao embarcacao[] = new Embarcacao[4];
		
		embarcacao[0] = new Embarcacao("J Porto #1", "Designacao #1", 2000, 1000);
		embarcacao[1] = new Embarcacao("D Porto #2", "Designacao #2", 2200, 2000);
		embarcacao[2] = new Embarcacao("A Porto #3", "Designacao #3", 2300, 3000);
		embarcacao[3] = new Cruzeiro("O Porto #4", "Designacao #4", 2400, 4000, 20);
		
		imprimir(embarcacao);
		System.out.println(System.lineSeparator() + "-------------------" + System.lineSeparator());
		System.out.println(maiorEmbarcacao(embarcacao));
		System.out.println(System.lineSeparator() + "-------------------" + System.lineSeparator());
		System.out.println(mediaCarga(embarcacao));
		System.out.println(System.lineSeparator() + "-------------------" + System.lineSeparator());
		
		//9. Obter a lista de barcos ordenada alfabeticamente.
		System.out.println("Lista ordenada alfabeticamente:" + System.lineSeparator());
		ordenarLista(embarcacao);
		imprimir(embarcacao);	
		
		//10. Criar um método para adicionar passageiros aos cruzeiros.
		System.out.println(System.lineSeparator() + "-------------------" + System.lineSeparator());
		System.out.println("Added 30 passageiros to cruzeiro");
		((Cruzeiro) embarcacao[3]).addPassageiros(30);		
		System.out.println(System.lineSeparator() + embarcacao[3]);
		
	}

}
