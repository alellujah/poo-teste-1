package portNow_revisoes;

//1. Crie uma classe que represente a entidade Embarcação e que contenha o porto de registo, 
//a designação, a capacidade de carga (em toneladas) e o comprimento (em metros). 
//A classe tem de incluir os métodos set e get respectivos.

public class Embarcacao {
	private String portoRegisto;
	private String designacao;
	private int carga;
	private int comprimento;
	
	//2. Na classe anterior crie um construtor que receba os atributos todos como argumentos.
	public Embarcacao(String portoRegisto, String designacao, int carga, int comprimento) {
		this.setPortoRegisto(portoRegisto);
		this.setDesignacao(designacao);
		this.setCarga(carga);
		this.setComprimento(comprimento);
	}
	

	public String getPortoRegisto() {
		return portoRegisto;
	}


	public void setPortoRegisto(String portoRegisto) {
		this.portoRegisto = portoRegisto;
	}


	public String getDesignacao() {
		return designacao;
	}


	public void setDesignacao(String designacao) {
		this.designacao = designacao;
	}


	public int getCarga() {
		return carga;
	}


	public void setCarga(int carga) {
		this.carga = carga;
	}


	public int getComprimento() {
		return comprimento;
	}


	public void setComprimento(int comprimento) {
		this.comprimento = comprimento;
	}
	
	//3. Na mesma classe crie um método toString() com a funcionalidade usual.
	@Override
	public String toString() {
		return "Porto de Registo: " + this.getPortoRegisto() + System.lineSeparator() +  
			   "Designacao: " + this.getDesignacao() + System.lineSeparator() + 
			   "Carga: " + this.getCarga() + System.lineSeparator() + 
			   "Comprimento: " + this.getComprimento() + System.lineSeparator(); 
	}

}
