package portNow_revisoes;

//8. Pretende distinguir-se os navios de cruzeiro dos restantes visto ser necessário conhecer-se a 
//lotação (em número de passageiros) destes navios. Programe o código necessário para tal.
public class Cruzeiro extends Embarcacao {
	private int numeroPassageiros;
	private int passageiro;
	
	public Cruzeiro(String portoRegisto, String designacao, int carga, int comprimento, int numeroPassageiros) {
		super(portoRegisto,designacao,carga,comprimento);
		this.setNumeroPassageiros(numeroPassageiros);
	}

	public int getNumeroPassageiros() {
		return numeroPassageiros;
	}

	public void setNumeroPassageiros(int numeroPassageiros) {
		this.numeroPassageiros = numeroPassageiros;
	}
	
	//10. Criar um método para adicionar passageiros aos cruzeiros.
	public void addPassageiros(int passageiros) {
		this.numeroPassageiros = this.getNumeroPassageiros() + passageiros;		
    }
	
	@Override
	public String toString() {
		return "Porto de Registo: " + this.getPortoRegisto() + System.lineSeparator() +  
			   "Designacao: " + this.getDesignacao() + System.lineSeparator() + 
			   "Carga: " + this.getCarga() + System.lineSeparator() + 
			   "Comprimento: " + this.getComprimento() + System.lineSeparator() + 
			   "Número de passageiros: " + this.getNumeroPassageiros();
	}
}
